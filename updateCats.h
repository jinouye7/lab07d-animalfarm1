/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file updateCats.h
/// @version 1.0
///
/// @author Jared Inouye <jinouye7@hawaii.edu>
/// @date 02_Mar_2022
/////////////////////////////////////////////////////////////////////////////
//


int updateCatName(const int index, const char newName[]);
int fixCat(const int index);
int updateCatWeight(const int index, const float newWeight);
int updateCatCollar1(const int index, const unsigned int newColor1);
int updateCatCollar2(const int index, const unsigned int newColor2);
int updateLicense(const int index, const unsigned long long license);
int updateBirthday(const int index, const char *date);

 

