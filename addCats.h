/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///
/// @file addCats.h
/// @version 1.0
///
/// @author Jared Inouye <jinouye7@hawaii.edu>
/// @date 02_Mar_2022
/////////////////////////////////////////////////////////////////////////////
//

#include <stdbool.h>
#include "catDatabase.h"

int addCat(const char name[], const int gender, const int breed, const bool isFixed, const float weight, const unsigned int collarColor1, const unsigned int collarColor2, const unsigned long long license, const char *date);




